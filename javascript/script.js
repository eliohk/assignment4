// DOM elements
let balance = document.getElementById('balance');
let pay = document.getElementById('pay');
let loanBtn = document.getElementById('getLoan');
let bankBtn = document.getElementById('bankBtn');
let workBtn = document.getElementById('workBtn');
let laptops = document.getElementById('laptops');
let features = document.getElementById('features');
let laptopImg = document.getElementById('laptopImg');
let laptopTitle = document.getElementById('laptopTitle');
let laptopDesc = document.getElementById('laptopDesc');
let laptopPrice = document.getElementById('laptopPrice');
let buyBtn = document.getElementById('buyBtn');
let payLoanBtn = document.getElementById('payLoanBtn');
let remainingLoan = document.getElementById('remainingLoan');
let loanText = document.getElementById('loanValue');

// boolean
let payFullLoan = false;

// local "cache" to reduce queries to the API
const map = new Map();

const fetchLaptops = (async () => {
    let url = 'https://hickory-quilled-actress.glitch.me/computers';
    await fetch(url)
    .then(async (response) => await response.json())
    .then((data) => {
        data.forEach(element => {
            // create dropdown options
            var option = document.createElement("option");
            option.value = element.id;
            option.innerHTML = element.title;
            laptops.appendChild(option);
            // add to map
            map.set(element.id, element);
        });
    });

    // invoke a "change"-event to execute laptopHandler();
    laptops.dispatchEvent(new Event('change'));
});

// Handlers

// Handles loans and its business logic
const loanHandler = (() => {
    if (payLoanBtn.hidden === false) {
        alert("You already have a loan. Kindly repay it in full before requesting a new loan. If not, we will bomb your house \n\nxoxo")
        return;
    }

    let validBalance = parseInt(balance.innerText);
    const validLoan = validBalance*2;
    let loanValue = prompt("Enter the amount you wish to loan.", validLoan);

    while (loanValue != null && !(loanValue > 0 && loanValue <= validLoan)) {
        loanValue = prompt(`You cannot loan more than twice your balance (${validLoan})`, validLoan);
    }

    if (loanValue == null) {
        return;
    }

    enableLoanUI();

    balance.innerText = validBalance + parseInt(loanValue);
    loanText.innerText = loanValue;
});

// Handles banking the money generated from work and its business logic
const bankHandler = (() => {
    if (payLoanBtn.hidden === false) {
        let validLoan = parseInt(loanText.innerText);
        let validPay = parseInt(pay.innerText);

        loanText.innerText = validLoan - (validPay * 0.1);
        pay.innerText = 0;

        let newLoanValue = parseInt(loanText.innerText);
        if (newLoanValue <= 0) {

            balance.innerText = parseInt(balance.innerText) + (validPay*0.9);

            disableLoanUI();
            alert("Your loan is paid! You have been granted access to another loan if needed.")
        }

        return;
    } 

    balance.innerText = parseInt(balance.innerText) +  parseInt(pay.innerText);
    pay.innerText = 0;
});

// Increases amount of money by 100 every click on work button.
const workHandler = (() => {
    pay.innerText = parseInt(pay.innerText) + 100;
});

// Handles buying a laptop and its business logic
const buyHandler = (() => {
    let validBalance = parseInt(balance.innerText);
    let price = parseInt(laptopPrice.innerText);

    if (validBalance >= price) {
        balance.innerText = validBalance - price;
    } else {
        alert("You are poor and can therefore not afford the luxury of a computer prestigious as this one.")
    }
});

// Handles how the user chooses to pay loan by editing button and boolean
const payLoanHandler = (() => {
    let validLoan = parseInt(loanText.innerText);
    let validPay = parseInt(pay.innerText);
    
    loanText.innerText = validLoan - validPay;
    pay.innerText = 0;

    let newLoanValue = parseInt(loanText.innerText);
    if (newLoanValue <= 0) {

        balance.innerText = parseInt(balance.innerText) + Math.abs(newLoanValue);

        disableLoanUI();
        alert("Your loan is paid! You have been granted access to another loan if needed.")
    }
});

// Handles which laptop metadata is shown depending on dropdown-menu
const laptopHandler = ((event) => {
    let id = parseInt(event.target.value);
    let laptop = map.get(id);

    features.innerText = "";
    laptop.specs.forEach(spec => {
        features.innerText += ` - ${spec}\n` 
    });

    laptopImg.src = `https://hickory-quilled-actress.glitch.me/${laptop.image}`;
    laptopTitle.innerText = laptop.title;
    laptopDesc.innerText = laptop.description;
    laptopPrice.innerText = laptop.price;    
});


// Eventlisteners
loanBtn.addEventListener('click', loanHandler);
bankBtn.addEventListener('click', bankHandler);
workBtn.addEventListener('click', workHandler);
laptops.addEventListener('change', laptopHandler);
buyBtn.addEventListener('click', buyHandler)
payLoanBtn.addEventListener('click', payLoanHandler);

// function calls
fetchLaptops();

// utils
function disableLoanUI() {
    payLoanBtn.hidden = true;
    remainingLoan.hidden = true;
    loanText.hidden = true;

}

function enableLoanUI() {
    payLoanBtn.hidden = false;
    remainingLoan.hidden = false;
    loanText.hidden = false;
}
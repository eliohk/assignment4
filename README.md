# Assignment 4 - The Komputer Store

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

This assignment is about creating a dynamic webpage using "vanilla" JavaScript. We are to follow the guidelines given in the task, but we are allowed to customize the app freely.



## Table of Contents

- [Assignment 4 - The Komputer Store](#assignment-4---the-komputer-store)
  - [Table of Contents](#table-of-contents)
  - [Install](#install)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contributing](#contributing)
  - [License](#license)

## Install

```
- Clone GitLab repository
```

## Usage

```
- Open 'index.html' in a browser (Preferably Google Chrome)
```

## Maintainers

[@eliohk](https://github.com/eliohk)

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

MIT © 2023 Khoi
